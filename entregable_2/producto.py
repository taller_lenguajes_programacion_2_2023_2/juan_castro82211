from conexion import Conexion
import pandas as pd

class Producto(Conexion):
    
    def __init__(self, id=0, nombre="", tipo_cubo="", precio=0, marca="", proveedor=""):
        """
        Este es el inicializador de la clase producto

        Args:
            id (int, optional): El identificador único del producto. Defaults to 0.
            nombre (str, optional): El cubo que se tiene como producto. Defaults to "".
            tipo_cubo (str, optional): El tipo de cubo que es el producto, por ejemplo, 3x3 o megaminx. Defaults to "".
            precio (int, optional): El precio del producto. Defaults to 0.
            marca (str, optional): La marca del producto. Defaults to "".
            proveedor (str, optional): El proveedor del producto, si es traido de fabrica es igual que la marca. Defaults to "".
        """
        self.__id = id
        self.__nombre = nombre
        self.__tipo_cubo = tipo_cubo
        self.__precio = precio
        self.__marca = marca
        self.__proveedor = proveedor

        super().__init__()
        self.create_Pro()
        ruta = "./entregable_2/static/xslx/padre_hijo.xlsx"
        self.df = pd.read_excel(ruta, sheet_name="Clase_padre")
        self.insert_Pro()

    def create_Pro(self):
        """Metodo para crear una tabla productos

        """
        if self.crear_tabla(nom_tabla="productos", datos_tbl="datos_pro"):
            print("Tabla Productos Creada!!!")
        
    def insert_Pro(self):
        """Metodo para insertar los productos dentro de la tabla

        """
        datos = ""
        
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}",{},"{}","{}"'.format(row["id"], row["cubo"], row["tipo_cubo"], row["precio"], row["marca"], row["proveedor"])
            self.insertar_datos(nom_tabla="productos", nom_columns="carga_pro", datos_carga=datos)
        return True
    
    def delete_Pro(self, nom_tabla, conditions):
        """Metodo para borrar un producto de la tabla

        Args:
            nom_tabla (_type_): la tabla de la cual se borrara el dato
            conditions (_type_): las condiciones requeridas para el dato que sera borrado
        """

        if self.borrar_datos(nom_tabla, conditions):
            print("Producto borrado con éxito.")
        else:
            print("Fallo al borrar el producto.")
        return True
    
    def update_Pro(self, nom_tabla, update_values, conditions):
        """Metodo para actualizar un producto en la tabla

        Args:
            nom_tabla (_type_): el nombre de la tabla a la cual se le actualizara el dato
            update_values (_type_): el dato a actualizar dentro de la tabla
            conditions (_type_): las condiciones requeridas para el dato que sera actualizado
        """
        if self.actualizar_datos(nom_tabla, update_values, conditions):
            print("Dato actualizado con éxito.")
        else:
            print("Fallo al actualizar dato.")
        return True

    # Métodos getter
    def get_id(self):
        return self.__id

    def get_cubo(self):
        return self.__cubo

    def get_tipo_cubo(self):
        return self.__tipo_cubo

    def get_precio(self):
        return self.__precio

    def get_marca(self):
        return self.__marca

    def get_proveedor(self):
        return self.__proveedor

    # Métodos setter
    def set_id(self, id):
        self.__id = id

    def set_cubo(self, cubo):
        self.__cubo = cubo

    def set_tipo_cubo(self, tipo_cubo):
        self.__tipo_cubo = tipo_cubo

    def set_precio(self, precio):
        self.__precio = precio

    def set_marca(self, marca):
        self.__marca = marca

    def set_proveedor(self, proveedor):
        self.__proveedor = proveedor

