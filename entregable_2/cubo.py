import pandas as pd
from producto import Producto

class Cubo(Producto):

    def __init__(self, nombre="", id=0, tipo_cubo="", precio=0, marca="", proveedor=""):
        
        """
        Este es el inicializador de la clase Cubo, hijo de la clase producto

        Args:
            id (int, optional): El identificador único del cubo, heredado de producto. Defaults to 0.
            tipo_cubo (str, optional): El tipo de cubo que es el producto, por ejemplo, 3x3 o megaminx. Heredado de producto. Defaults to "".
            precio (int, optional): El precio del cubo, heredado de producto. Defaults to 0.
            marca (str, optional): La marca del cubo, heredada de producto. Defaults to "".
            proveedor (str, optional): El proveedor del cubo, si es traído de fábrica es igual que la marca. Heredado de producto. Defaults to "".
            descripcion (str, optional): Una descripción del cubo. Defaults to "".
            color_fondo (str, optional): El color de fondo del cubo. Defaults to "".
            dimensiones (str, optional): Las dimensiones del cubo en centimetros. Defaults to "".
            forma (str, optional): La forma del cubo. Defaults to "".
            magnetico (str, optional): Indica si el cubo es magnético o no. Defaults to "".
            peso (int, optional): El peso del cubo en gramos. Defaults to 0.
        """
        super().__init__(id, nombre, tipo_cubo, precio, marca, proveedor)

    def init2(self, descripcion="", color_fondo="", dimensiones="", forma="", magnetico="",peso=0):
        self.descripcion = descripcion
        self.color_fondo = color_fondo
        self.dimensiones = dimensiones
        self.forma = forma
        self.magnetico = magnetico
        self.peso = peso

        self.create_Cubo()
        ruta = "./entregable_2/static/xslx/padre_hijo.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Clase_hijo")
        self.insert_Cubo()
        
    def create_Cubo(self):
        """Metodo para crear una tabla cubos

        """
        if self.crear_tabla(nom_tabla="cubos", datos_tbl="datos_cub"):
            print("Tabla Cubos Creada!!!")
        
    def insert_Cubo(self):
        """Metodo para insertar los cubos dentro de la tabla

        """
        datos = ""
        
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}",{},"{}","{}","{}","{}","{}","{}","{}",{}'.format(row["id"], row["cubo"], row["tipo_cubo"], row["precio"], row["marca"], row["proveedor"],
                                                                                   row["descripcion"],row["color_fondo"],row["dimensiones"],row["forma"],row["magnetico"],row["peso"])
            self.insertar_datos(nom_tabla="cubos", nom_columns="carga_cub", datos_carga=datos)
        return True
    
    def delete_Cub(self, nom_tabla, conditions):
        """Metodo para borrar un producto de la tabla

        Args:
            nom_tabla (_type_): la tabla de la cual se borrara el dato
            conditions (_type_): las condiciones requeridas para el dato que sera borrado
        """

        if self.borrar_datos(nom_tabla, conditions):
            print("Producto borrado con éxito.")
        else:
            print("Fallo al borrar el producto.")
        return True
    
    def update_Cub(self, nom_tabla, update_values, conditions):
        """Metodo para actualizar un producto en la tabla

        Args:
            nom_tabla (_type_): el nombre de la tabla a la cual se le actualizara el dato
            update_values (_type_): el dato a actualizar dentro de la tabla
            conditions (_type_): las condiciones requeridas para el dato que sera actualizado
        """
        if self.actualizar_datos(nom_tabla, update_values, conditions):
            print("Dato actualizado con éxito.")
        else:
            print("Fallo al actualizar dato.")
        return True
    
    # Métodos getter para los atributos de Cubo
    def get_id(self):
        return self.id

    def get_nombre(self):
        return self.nombre

    def get_tipo_cubo(self):
        return self.tipo_cubo

    def get_precio(self):
        return self.precio

    def get_marca(self):
        return self.marca

    def get_proveedor(self):
        return self.proveedor
    
    def get_descripcion(self):
        return self.__descripcion

    def get_color_fondo(self):
        return self.__color_fondo

    def get_dimensiones(self):
        return self.__dimensiones

    def get_forma(self):
        return self.__forma

    def get_magnetico(self):
        return self.__magnetico

    def get_peso(self):
        return self.__peso

    # Métodos setter para los atributos de Cubo
    def set_id(self, id):
        self.id = id

    def set_nombre(self, nombre):
        self.nombre = nombre

    def set_tipo_cubo(self, tipo_cubo):
        self.tipo_cubo = tipo_cubo

    def set_precio(self, precio):
        self.precio = precio

    def set_marca(self, marca):
        self.marca = marca

    def set_proveedor(self, proveedor):
        self.proveedor = proveedor

    def set_descripcion(self, descripcion):
        self.__descripcion = descripcion

    def set_color_fondo(self, color_fondo):
        self.__color_fondo = color_fondo

    def set_dimensiones(self, dimensiones):
        self.__dimensiones = dimensiones

    def set_forma(self, forma):
        self.__forma = forma

    def set_magnetico(self, magnetico):
        self.__magnetico = magnetico

    def set_peso(self, peso):
        self.__peso = peso