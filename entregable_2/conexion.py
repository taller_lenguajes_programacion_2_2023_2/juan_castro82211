import sqlite3
import json

class Conexion:
    def __init__(self) -> None:
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_tienda_cubos.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()
    
    def obtener_json(self):

        """
        Lee un archivo JSON que contiene consultas SQL y devuelve un diccionario con las consultas.

        Returns:
            querys, el diccionario de consultas SQL.
        """

        ruta = "./entregable_2/static/sql/querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        """
        Crea una nueva tabla dentro de la base de datos sqlite

        Args:
            nom_tabla (str, optional): El nombre de la nueva tabla. Defaults to "".
            datos_tbl (str, optional): Los datos que tendra la tabla nueva, traidos desde el json. Defaults to "".
        """
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="",nom_columns = "", datos_carga=""):
        """Inserta datos dentro de una tabla existente

        Args:
            nom_tabla (str, optional): El nombre de la tabla en la que se insertaran los datos. Defaults to "".
            nom_columns (str, optional): Los nombres de las columnas de la tabla. Defaults to "".
            datos_carga (str, optional): Los datos a insertar dentro de la tabla. Defaults to "".
        """
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
        
    def borrar_datos(self, nom_tabla, conditions):
        """
        Borra datos en una tabla basado en ciertas condiciones

        Args:
            nom_tabla (_type_): El nombre de la tabla de donde se borrara el dato
            conditions (_type_): Las condiciones requeridas para borrar el dato de la tabla. (Ejemplo: "id = 1").
        """
        if nom_tabla and conditions:
            query = self.__querys["delete"].format(nom_tabla,conditions)
            try:
                self.__cursor.execute(query)
                self.__conex.commit()
                return True
            except sqlite3.Error as e:
                print(f"Error deleting data: {e}")
                return False
        else:
            return False
        
    def actualizar_datos(self, nom_tabla, update_values, conditions):
        """
        Actualiza datos en una tabla basado en ciertas condiciones

        Args:
            nom_tabla (str): El nombre de la tabla en la que se actualizara el dato.
            update_values (str): Los nuevos valores a actualizar. (Ejemplo: "valor" = 42).
            conditions (str): Las condiciones que debe tener la fila para actualizar un valor. (Ejemplo: "id = 1").

        """
        if nom_tabla and update_values and conditions:
            query = self.__querys["update"].format(nom_tabla, update_values, conditions)
            try:
                self.__cursor.execute(query)
                self.__conex.commit()
                return True
            except sqlite3.Error as e:
                print(f"Error updating data: {e}")
                return False
        else:
            return False

