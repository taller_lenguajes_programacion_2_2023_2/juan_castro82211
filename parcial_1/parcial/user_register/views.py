from django.shortcuts import render, redirect
from datetime import datetime
from .forms import PersonaRegisterForm, UserRegisterForm
from .models import Usuario

def home(request):

    return render(request, "home.html")

def registro_persona(request):

    if request.method == 'POST':
        form = PersonaRegisterForm(request.POST)
        if form.is_valid():

            usuario = Usuario(
                nom1=form.cleaned_data['nom1'],
                nom2=form.cleaned_data['nom2'],
                apell1=form.cleaned_data['apell1'],
                apell2=form.cleaned_data['apell2'],
                f_nac=form.cleaned_data['f_nac'],
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password1'],
                f_registro=datetime.now(),
                token=1
            )

            usuario.save()
            return redirect('login')

    else:
        form = PersonaRegisterForm()

    context = {'form' : form}
    return render(request, "registro_persona.html", context)

def registro(request):

    if request.method == 'POST':

        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            return redirect('login')
    else:
        form = UserRegisterForm()

    context = { 'form' : form }     

    return render(request, "registro.html", context)