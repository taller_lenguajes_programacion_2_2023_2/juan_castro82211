from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Usuario

class PersonaRegisterForm(UserCreationForm):

    nom1 = forms.CharField(label = "Primer nombre")
    nom2 = forms.CharField(label = "Segundo nombre", required=False)
    apell1 = forms.CharField(label = "Primer apellido")
    apell2 = forms.CharField(label = "Segundo apellido")
    f_nac = forms.DateField(label = "Fecha de nacimiento", widget=forms.DateInput(attrs={'type': 'date'}))
    email = forms.EmailField(label = "Email")
    password1 = forms.CharField(label = "contrasena", widget= forms.PasswordInput)
    password2 = forms.CharField(label = "confirmar contrasena", widget= forms.PasswordInput)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if Usuario.objects.filter(email=email).exists():
            raise forms.ValidationError("Esta direccion de correo ya esta en uso.")
        return email
    
    def clean_username(self):
        username = self.cleaned_data.get('username')
        if Usuario.objects.filter(username=username).exists():
            raise forms.ValidationError("Este nombre de usuario ya esta en uso.")
        return username

    class Meta:
        model = User
        fields = ['nom1', 'nom2', 'apell1', 'apell2', 'f_nac', 'email', 'password1', 'password2']
        help_texts = {k:"" for k in fields}

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        help_texts = {k:"" for k in fields}
