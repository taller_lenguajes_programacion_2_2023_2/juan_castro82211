from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path("", views.home, name="home"),
    path("registro_persona/", views.registro_persona, name="registro_persona"),
    path("registro/", views.registro, name="registro"),
    path("login/", LoginView.as_view(template_name='login.html'), name="login"),
]