from django.db import models

class Persona(models.Model):

    id = models.IntegerField(primary_key=True)
    nom1 = models.CharField(max_length=100)
    nom2 = models.CharField(max_length=100, blank=True, null=True)
    apell1 = models.CharField(max_length=100)
    apell2 = models.CharField(max_length=100)
    f_nac = models.DateField()

class Usuario(Persona):
    
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=30)
    f_registro = models.DateField()
    token = models.IntegerField()

