function checkLocalStorageItem(itemName) {
    // Check if the item exists in local storage
    if (localStorage.getItem(itemName)) {
        
        document.getElementById('login_text').innerHTML = "Bienvenido " + localStorage.getItem('nombre_cuenta');
        document.getElementById('registro_text').innerHTML = "Cerrar sesión";
    
        const anchor = document.getElementById('login_text');
        const clear = document.getElementById('registro_text');
        anchor.addEventListener('click', function(event) {
            event.preventDefault(); // Prevent the default action, such as navigating to a new page
          });
          anchor.style.cursor = 'text';
          anchor.title = 'Nombre completo: ' + localStorage.getItem('nom_completo_cuenta') + '    Correo: ' + localStorage.getItem('email_cuenta');

        clear.addEventListener('click', function () {

            clear.href = 'login.html';
            localStorage.removeItem('logged_true'); 
        });
    } 
}

checkLocalStorageItem('logged_true');

const carritoButton = document.getElementById('carrito_btn');
const squareOverlay = document.getElementById('squareOverlay');
const closeSquareButton = document.getElementById('closeSquare');

// Show the square when the "Carrito" button is clicked
function showSquare(){

    squareOverlay.style.display = 'flex';
}

// Close the square when the "Close" button is clicked
closeSquareButton.addEventListener('click', () => {
    squareOverlay.style.display = 'none';
});

let counter = 0;
let precioTotal = 0;
const carritoVacio = document.getElementById('vacio');

function addItem(button) {
    const itemList = document.getElementById('itemList');
    const newItem = document.createElement('li');
    const deleteButton = document.createElement('button'); // Create a delete button
    const parentAnchor = button.parentElement.parentElement;
    const parentDiv = button.parentElement;
    const title = parentAnchor.getAttribute('title');
    const precioElement = parentDiv.querySelector('.precio');
    const priceText = precioElement.textContent.trim();
    const priceValue = parseFloat(priceText.replace(/[^0-9]/g, ''));
    const precioTotalElement = document.getElementById('precioTotal');
    const itemText = title + ' - ' + priceText;

    deleteButton.textContent = 'Eliminar'; // Set the text for the delete button
    deleteButton.className = 'delete-button';
    deleteButton.addEventListener('click', () => {
        // Call the deleteItem function when the delete button is clicked
        deleteItem(newItem);
    });

    newItem.textContent = itemText;
    newItem.className = counter % 2 === 0 ? "even" : "odd";
    
    // Append the delete button to the list item
    newItem.appendChild(deleteButton);
    
    itemList.appendChild(newItem);
    precioTotal = precioTotal + priceValue;
    precioTotalElement.textContent = `Precio total: $${precioTotal}`;
    counter++;

    if (counter >= 1) {
        carritoVacio.style.display = 'none';
    }
}

function deleteItem(item) {
    const itemList = document.getElementById('itemList');
    const precioTotalElement = document.getElementById('precioTotal');
    const itemText = item.textContent;
    const priceText = itemText.split(' - ')[1];
    const priceValue = parseFloat(priceText.replace(/[^0-9]/g, ''));
    
    itemList.removeChild(item); // Remove the item from the list
    precioTotal = precioTotal - priceValue; // Deduct the price from the total
    precioTotalElement.textContent = `Precio total: $${precioTotal}`;
    
    if (itemList.childElementCount === 0) {
        carritoVacio.style.display = 'block'; // Display "El carrito está vacío" if the cart is empty
    }
}

const itemList = document.getElementById('itemList');
itemList.addEventListener('click', (event) => {
    if (event.target.tagName === 'BUTTON' && event.target.classList.contains('delete-button')) {
        // If the clicked element is a button with the 'delete-button' class, delete the corresponding item
        deleteItem(event.target.parentElement);
    }
});

let slideIndex = [1,1];
let slideId = ["mySlides1", "mySlides2"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
  let i;
  let x = document.getElementsByClassName(slideId[no]);
  if (n > x.length) {slideIndex[no] = 1}    
  if (n < 1) {slideIndex[no] = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex[no]-1].style.display = "block";  
}


