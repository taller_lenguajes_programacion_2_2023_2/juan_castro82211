## INTRODUCCION

Este proyecto tiene una página web con registro,login y listado de productos, cada uno con su vista respectiva

## CARACTERISTICAS

- Registro de datos de persona
- Login con correo y contraseña
- Vista de detalles de cada uno de los productos

## INSTRUCCIONES

1. Crear proyecto entregable3
2. Crear app webinicio
3. Crear app weblogin
4. Crear app webventa
5. Agregar apps a settings en entregable3
6. Definir ruta de static en settings
7. Crear la vista de webinicio 
8. Configurar la ruta de webinicio
9. Crear index.html 
10. Crear modelos Persona y Usuario
11. Crear los formularios de registro y login
12. Crear las vistas de login y registro
13. Agregar las rutas a las url
14. Crear login.html y registro.html
15. Crear vista de webventa 
16. Agregar la vista a las url
17. Crear el modelo de productos
18. Crear lista.html
19. Crear el html para el detalle de los productos