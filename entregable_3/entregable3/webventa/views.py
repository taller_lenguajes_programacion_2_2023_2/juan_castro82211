from django.shortcuts import render
from .models import Productos

def lista(request):

    """Vista de productos"""
    n_productos = Productos.objects.count() # Sacar el numero de productos en una variable
    lista_prod = Productos.objects.all() # Sacar la lista de productos en una variable

    return render(request,'lista.html',{'n_prod':n_productos,'lista_p':lista_prod})

def detalle_producto(request, producto_id):
    
    """Vista del detalle del producto segun la id del mismo"""
    producto = Productos.objects.get(id=producto_id)
    
    return render(request, 'detalle_prod.html', {'prod': producto})