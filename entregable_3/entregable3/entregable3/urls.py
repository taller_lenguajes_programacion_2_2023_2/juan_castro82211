from django.contrib import admin
from django.urls import path
from webinicio.views import inicio
from weblogin.views import login, registro
from webventa.views import lista, detalle_producto

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', inicio, name='inicio'),
    path('login/', login, name='login'),
    path('registro/', registro, name='registro'),
    path('lista/', lista, name='lista'),
    path('producto/<int:producto_id>/', detalle_producto, name='detalle_producto'),
]

